package task1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.NoSuchElementException;
import java.util.Scanner;

class YearOfEaster {

    private int year;


    // #1st method for input DataYear (Year as parameter in constructor)
    public YearOfEaster(int year) {
        this.year = year;
    }

    public YearOfEaster() {
    }

    // #2nd method for input DataYear (Scanner(System.in) - Keybord) .
    void scannerKeybord() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Set Digits as a Year.");
        this.year = sc.nextInt();
    }

    // #3rd method for input DataYear (Read from file)
    void readFromFile() {
        try {
            Scanner in = new Scanner(new File("src/task1/task1"));
            this.year = Integer.parseInt(in.nextLine());
        } catch (FileNotFoundException e) {
            System.out.println("Error. File to read is not exist.");
            System.exit(0);
        } catch (NoSuchElementException e) {
            System.out.println("Error. Data of Year in file is empty");
            System.exit(0);
        } catch (NumberFormatException e) {
            System.out.println("Error. Wrong format data of Year");
            System.exit(0);
        }
    }

    void mathOfEaster() {
        int a = year % 19;
        int b = year / 100;
        int c = year % 100;
        int d = b / 4;
        int e = b % 4;
        int f = (b + 8) / 25;
        int g = (b - f + 1) / 3;
        int h = ((19 * a) + b - d - g + 15) % 30;
        int i = c / 4;
        int k = c % 4;
        int l = (32 + (2 * e) + (2 * i) - h - k) % 7;
        int m = (a + (11 * h) + (22 * l)) / 451;
        int p = (h + l - (7 * m) + 114) % 31;
        int day = p + 1;
        if (((h + l - (7 * m) + 114) / 31) == 4) {
            System.out.println("Date of Easter in " + year + " year: " + day + " April " + year);
        } else
            System.out.println("Date of Easter in " + year + ": " + day + " March " + year);
    }
}
